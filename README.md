Unity 2017
GoogleVR SDK 1.70

HTC Vive headset model by Eternal Realm - CC BY 4.0

[HTC Vive by Eternal Realm - download 3D scene - Sketchfab](https://sketchfab.com/models/4cee0970fe60444ead77d41fbb052a33)

HTC Vive Tracker model by MarcoRomero - CC BY 4.0

[HTC Vive Tracker by MarcoRomero - download 3D scene - Sketchfab](https://sketchfab.com/models/4bcb460ac22248f7abf4beeacae954e3)

HTC Vive Controller model by MarcoRomero

https://sketchfab.com/models/7199d01cef484c7ba67e0cf8ca690d97

Floater v0.0.2 script by Donovan Keith